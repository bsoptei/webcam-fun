const video = document.querySelector("video");
const template = document.getElementById("template");
const canvas = document.getElementById("render");
const ctxTemplate = template.getContext("2d");
const ctxRender = canvas.getContext("2d");
const width = 1920;
const height = 1080;
const FPS = 25;
template.width = width;
template.height = height;
canvas.width = width;
canvas.height = height;
ctxRender.globalAlpha = 0.4;

navigator.mediaDevices
    .getUserMedia({
        audio: false,
        video: true
    })
    .then(stream => {
        window.stream = stream;
        video.srcObject = stream;
    })
    .catch(error => {
        console.log("Failed to access webcam: ", error.message, error.name);
    });


function randomInt(min, max) {
    return parseInt(Math.random() * (max - min + 1), 10) + min
}

setInterval(() => {
    ctxTemplate.drawImage(video, 0, 0, canvas.width, canvas.height);
    const imageData = ctxTemplate.getImageData(0, 0, width, height);

    var pixels = imageData.data;
    var w = imageData.width;
    var h = imageData.height;
    var l = w * h;

    ctxRender.fillStyle = "black";
    ctxRender.fillRect(0, 0, canvas.width, canvas.height);

    for (var i = 0; i < l; i += randomInt(39, 71)) {
        var y = parseInt(i / w, 10);
        var red = pixels[i * 4];
        var green = pixels[i * 4 + 1];
        var blue = pixels[i * 4 + 2];
        var x = i - y * w;

        if (red < 50 && green < 50 && blue < 50) {
            if (Math.random() < 0.5) {
                ctxRender.font = `${randomInt(10, 30)}px serif`;
                ctxRender.fillStyle = `rgb(0, 150, ${randomInt(10, 255)})`;
                ctxRender.fillText(" ?!."[randomInt(0, 3)], x, y);
            } else {
                var size = randomInt(1, 5);
                ctxRender.beginPath();
                ctxRender.ellipse(x, y, size, size, 0, 0, 2 * Math.PI);
                ctxRender.fill();
            }
        } else if (red > 200) {
            ctxRender.font = `${randomInt(10, 30)}px serif`;
            ctxRender.fillStyle = `rgb(0, 255, 0)`;
            ctxRender.fillText("Á", x, y);
        }
    }
}, 1000 / FPS);
